package com.jmarvie.animaFightSimulatorbackend.dao;

import com.jmarvie.animaFightSimulatorbackend.model.FighterGroup;
import com.jmarvie.animaFightSimulatorbackend.model.Master;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FighterGroupDao extends JpaRepository<FighterGroup, Long> {

    List<FighterGroup> findAllByMaster(Master master);

    FighterGroup findByIdGroup(Long id);

    void deleteByIdGroup(Long id);

    void deleteByMaster(Long idMaster);

}
