package com.jmarvie.animaFightSimulatorbackend.dao;

import com.jmarvie.animaFightSimulatorbackend.model.Master;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterDao extends JpaRepository<Master, Long> {

    Master findByLoginAndEmailAddress(String login, String emailAddress);

    Master findByEmailAddressAndPassword(String address, String password);

}
