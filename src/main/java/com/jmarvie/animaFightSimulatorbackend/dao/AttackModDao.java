package com.jmarvie.animaFightSimulatorbackend.dao;

import com.jmarvie.animaFightSimulatorbackend.model.AttackMod;
import com.jmarvie.animaFightSimulatorbackend.model.Fighter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttackModDao extends JpaRepository<AttackMod, Long>{

    List<AttackMod> findAllByFighter(Fighter fighter);

    AttackMod findByIdMod(long idMod);

    void deleteByIdMod(long idMod);

}
