package com.jmarvie.animaFightSimulatorbackend.dao;

import com.jmarvie.animaFightSimulatorbackend.model.Fighter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FighterDao extends JpaRepository<Fighter, Long>{

    List<Fighter> findAllByMaster(long idMaster);

    List<Fighter> findAllByGroup(long idGroup);

}
