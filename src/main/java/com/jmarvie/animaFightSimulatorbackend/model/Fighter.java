package com.jmarvie.animaFightSimulatorbackend.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Fighter implements Serializable {

	private static final long serialVersionUID = 1503253448925498843L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idFighter;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name="idMaster")
    private Master master;

    @OneToMany(targetEntity = AttackMod.class, mappedBy = "fighter", fetch = FetchType.LAZY)
    private List<AttackMod> mods;

    @ManyToMany(mappedBy="fighters")
    private List<FighterGroup> groups = new ArrayList<>();

    @Transient
    private int initScore;

    public Fighter() {
    }

    public long getIdFighter() {
        return idFighter;
    }

    public void setIdFighter(long idFighter) {
        this.idFighter = idFighter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Master getMaster() {
        return master;
    }

    public void setMaster(Master master) {
        this.master = master;
    }

    public List<FighterGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<FighterGroup> groups) {
        this.groups = groups;
    }

    public List<AttackMod> getMods() {
        return mods;
    }

    public void setMods(List<AttackMod> mods) {
        this.mods = mods;
    }

    public int getInitScore() {
        return initScore;
    }

    public void setInitScore(int initScore) {
        this.initScore = initScore;
    }
}
