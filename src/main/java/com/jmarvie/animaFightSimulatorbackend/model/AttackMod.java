package com.jmarvie.animaFightSimulatorbackend.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AttackMod implements Serializable {

	private static final long serialVersionUID = 546596619942648292L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idMod;

    @Column(nullable = false)
    private String name;

    @Column
    private int agiValue;

    @ManyToOne
    @JoinColumn(name="idFighter")
    private Fighter fighter;

    public AttackMod() {
    }

    public long getIdMod() {
        return idMod;
    }

    public void setIdMod(long idMod) {
        this.idMod = idMod;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAgiValue() {
        return agiValue;
    }

    public void setAgiValue(int agiValue) {
        this.agiValue = agiValue;
    }

    public Fighter getFighter() {
        return fighter;
    }

    public void setFighter(Fighter fighter) {
        this.fighter = fighter;
    }
}

