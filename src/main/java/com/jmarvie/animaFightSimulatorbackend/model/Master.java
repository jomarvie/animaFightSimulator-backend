package com.jmarvie.animaFightSimulatorbackend.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Master implements Serializable {

	private static final long serialVersionUID = 7675191332178784279L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idMaster;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false, unique = true)
    private String emailAddress;

    @Column(nullable = false)
    private String password;

    @OneToMany(targetEntity = Fighter.class, mappedBy="master")
    private List<Fighter> fighters;

    @OneToMany(targetEntity = FighterGroup.class, mappedBy="master")
    private List<FighterGroup> groups;

    public Master() {
    }

    public long getIdMaster() {
        return idMaster;
    }

    public void setIdMaster(long idMaster) {
        this.idMaster = idMaster;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Fighter> getFighters() {
        return fighters;
    }

    public void setFighters(List<Fighter> fighters) {
        this.fighters = fighters;
    }

    public List<FighterGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<FighterGroup> groups) {
        this.groups = groups;
    }

}
