package com.jmarvie.animaFightSimulatorbackend.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FighterGroup implements Serializable {

	private static final long serialVersionUID = -7810807421149777723L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idGroup;

    @Column(nullable = false)
    private String name;

    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name="idMaster")
    private Master master;

    @ManyToMany
    @JoinTable(name = "FighterGroup_List",
        joinColumns = {@JoinColumn(name="idGroup", referencedColumnName="idGroup")},
        inverseJoinColumns = {@JoinColumn(name="idFighter", referencedColumnName="idFighter")})
    private List<Fighter> fighters = new ArrayList<>();

    public long getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(long idGroup) {
        this.idGroup = idGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Master getMaster() {
        return master;
    }

    public void setMaster(Master master) {
        this.master = master;
    }

    public List<Fighter> getFighters() {
        return fighters;
    }

    public void setFighters(List<Fighter> fighters) {
        this.fighters = fighters;
    }
}
