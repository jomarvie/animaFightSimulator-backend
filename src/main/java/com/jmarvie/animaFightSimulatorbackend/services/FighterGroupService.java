package com.jmarvie.animaFightSimulatorbackend.services;


import com.jmarvie.animaFightSimulatorbackend.model.FighterGroup;

import java.util.List;

public interface FighterGroupService {

    FighterGroup findGroupById(long idGroup);

    List<FighterGroup> findAllGroupsByMaster(long idMaster);

    long createGroup(FighterGroup group);

    FighterGroup updateGroup(FighterGroup group);

    void deleteGroup(long idGroup);

}
