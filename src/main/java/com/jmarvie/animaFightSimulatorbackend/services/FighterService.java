package com.jmarvie.animaFightSimulatorbackend.services;

import com.jmarvie.animaFightSimulatorbackend.model.Fighter;

import java.util.List;

public interface FighterService {

    Fighter findFighterById(long idFighter);

    List<Fighter> findAllFightersByGroup(long idGroup);

    List<Fighter> findAllFightersByMaster(long idMaster);

    long createFighter(Fighter fighter);

    Fighter updateFighter(Fighter fighter);

    void deleteFighter(long idFighter);

}
