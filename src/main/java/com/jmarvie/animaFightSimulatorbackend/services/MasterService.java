package com.jmarvie.animaFightSimulatorbackend.services;

import com.jmarvie.animaFightSimulatorbackend.model.Master;

import java.util.List;

public interface MasterService {

    public List<Master> findAllMasters();

    public Master findMasterById(long id);

    public Master findMasterByLoginAndPassword(String login, String password);

    public Master findMasterByEmailAndPassword(String email, String password);

    public long createMaster(Master master);

    public Master updateMaster(Master master);

    public void deleteMaster(long id);

}
