package com.jmarvie.animaFightSimulatorbackend.services;


import com.jmarvie.animaFightSimulatorbackend.model.AttackMod;
import com.jmarvie.animaFightSimulatorbackend.model.Fighter;

import java.util.List;

public interface AttackModService {

    AttackMod findModById(long idMod);
    
	List<AttackMod> findAllByFighter(Fighter Fighter);

    long createMod(AttackMod mod);

    AttackMod updateMod(AttackMod mod);

    void deleteMod(long idMod);

}
