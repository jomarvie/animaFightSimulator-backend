package com.jmarvie.animaFightSimulatorbackend.services.impl;

import com.jmarvie.animaFightSimulatorbackend.dao.MasterDao;
import com.jmarvie.animaFightSimulatorbackend.model.Master;
import com.jmarvie.animaFightSimulatorbackend.services.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

@Service
public class MasterServiceImpl implements MasterService {

    @Autowired
    private MasterDao masterDao;

    @Override
    @Transactional
    public List<Master> findAllMasters() {
        return masterDao.findAll();
    }

    @Override
    @Transactional
    public Master findMasterById(long id) {
    	Master master = (Master) masterDao.getOne(id);
        return master;
    }

    @Override
    @Transactional
    public Master findMasterByLoginAndPassword(String login, String password) {
        return masterDao.findByLoginAndEmailAddress(login, password);
    }

    @Override
    @Transactional
    public Master findMasterByEmailAndPassword(String email, String password) {
        return masterDao.findByEmailAddressAndPassword(email, password);
    }

    @Override
    @Transactional
    public long createMaster(Master master) {
        return masterDao.save(master).getIdMaster();
    }

    @Override
    @Transactional
    public Master updateMaster(Master master) {
        return masterDao.save(master);
    }

    @Override
    @Transactional
    public void deleteMaster(long idMaster) {
    	masterDao.deleteById(idMaster);
    }
}
