package com.jmarvie.animaFightSimulatorbackend.services.impl;

import com.jmarvie.animaFightSimulatorbackend.dao.FighterDao;
import com.jmarvie.animaFightSimulatorbackend.model.Fighter;
import com.jmarvie.animaFightSimulatorbackend.services.FighterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

@Service
public class FighterServiceImpl implements FighterService{
	
	@Autowired
	private FighterDao fighterDao;

    @Override
    @Transactional
    public Fighter findFighterById(long idFighter) {
    	Fighter fighter = (Fighter) fighterDao.getOne(idFighter);
        return fighter;
    }

    @Override
    @Transactional
    public List<Fighter> findAllFightersByGroup(long idGroup) {
        return fighterDao.findAllByGroup(idGroup);
    }

    @Override
    @Transactional
    public List<Fighter> findAllFightersByMaster(long idMaster) {
        return fighterDao.findAllByMaster(idMaster);
    }

    @Override
    @Transactional
    public long createFighter(Fighter fighter) {
        return fighterDao.save(fighter).getIdFighter();
    }

    @Override
    @Transactional
    public Fighter updateFighter(Fighter fighter) {
        return fighterDao.save(fighter);
    }

    @Override
    @Transactional
    public void deleteFighter(long idFighter) {
    	fighterDao.deleteById(idFighter);
    }

}
