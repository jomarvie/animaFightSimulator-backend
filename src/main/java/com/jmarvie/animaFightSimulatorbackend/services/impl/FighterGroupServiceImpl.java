package com.jmarvie.animaFightSimulatorbackend.services.impl;

import com.jmarvie.animaFightSimulatorbackend.dao.FighterGroupDao;
import com.jmarvie.animaFightSimulatorbackend.model.FighterGroup;
import com.jmarvie.animaFightSimulatorbackend.services.FighterGroupService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

@Service
public class FighterGroupServiceImpl implements FighterGroupService {
	
	@Autowired
	private FighterGroupDao fighterGroupDao;

    @Override
    @Transactional
    public FighterGroup findGroupById(long idGroup) {
    	FighterGroup fighterGroup = (FighterGroup) fighterGroupDao.getOne(idGroup);
        return fighterGroup;
    }

    @Override
    @Transactional
    public List<FighterGroup> findAllGroupsByMaster(long idMaster) {
        return fighterGroupDao.findAll();
    }

    @Override
    @Transactional
    public long createGroup(FighterGroup group) {
        return fighterGroupDao.save(group).getIdGroup();
    }

    @Override
    @Transactional
    public FighterGroup updateGroup(FighterGroup group) {
        return fighterGroupDao.save(group);
    }

    @Override
    @Transactional
    public void deleteGroup(long idGroup) {
    	fighterGroupDao.deleteById(idGroup);
    }
}
