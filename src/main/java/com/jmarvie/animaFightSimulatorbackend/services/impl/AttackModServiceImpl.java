package com.jmarvie.animaFightSimulatorbackend.services.impl;

import com.jmarvie.animaFightSimulatorbackend.dao.AttackModDao;
import com.jmarvie.animaFightSimulatorbackend.model.AttackMod;
import com.jmarvie.animaFightSimulatorbackend.model.Fighter;
import com.jmarvie.animaFightSimulatorbackend.services.AttackModService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

@Service
public class AttackModServiceImpl implements AttackModService{
	
	@Autowired
	private AttackModDao attackModDao;

    @Override
    @Transactional
    public AttackMod findModById(long idMod) {
    	AttackMod mod = (AttackMod) attackModDao.getOne(idMod);
        return mod;
    }

    @Override
    @Transactional
    public List<AttackMod> findAllByFighter(Fighter fighter) {
        return attackModDao.findAllByFighter(fighter);
    }

    @Override
    @Transactional
    public long createMod(AttackMod mod) {
        return attackModDao.save(mod).getIdMod();
    }

    @Override
    @Transactional
    public AttackMod updateMod(AttackMod mod) {
        return attackModDao.save(mod);
    }

    @Override
    @Transactional
    public void deleteMod(long idMod) {
    	attackModDao.deleteById(idMod);
    }
}
