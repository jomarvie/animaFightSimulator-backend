package com.jmarvie.animaFightSimulatorbackend.controllers;

import com.jmarvie.animaFightSimulatorbackend.model.FighterGroup;
import com.jmarvie.animaFightSimulatorbackend.services.FighterGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FighterGroupController {

    @Autowired
    public FighterGroupService fighterGroupService;

    @GetMapping(value="/groups/{idMaster}", produces = "application/json")
    public List<FighterGroup> findFighterGroupListByMaster(@PathVariable long idMaster) {
        return fighterGroupService.findAllGroupsByMaster(idMaster);
    }

    @GetMapping(value="/groups/{idFighter}", produces = "application/json")
    public List<FighterGroup> findFighterGroupListByFighter(@PathVariable long idFighter) {
        return fighterGroupService.findAllGroupsByMaster(idFighter);
    }

    @PostMapping(value="/groups", produces = "application/json", consumes = "application/json")
    public long createGroup(@RequestBody FighterGroup group) {
        return fighterGroupService.createGroup(group);
    }

    @PutMapping(value="/groups", produces = "application/json", consumes = "application/json")
    public FighterGroup updateGroup(@RequestBody FighterGroup group) {
        return fighterGroupService.updateGroup(group);
    }

    @DeleteMapping(value = "/groups/{idGroup}")
    public void deleteGroup(@PathVariable long idGroup) {
        fighterGroupService.deleteGroup(idGroup);
    }
}
