package com.jmarvie.animaFightSimulatorbackend.controllers;

import com.jmarvie.animaFightSimulatorbackend.model.AttackMod;
import com.jmarvie.animaFightSimulatorbackend.model.Fighter;
import com.jmarvie.animaFightSimulatorbackend.services.AttackModService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AttackModController {

    @Autowired
    private AttackModService modService;

    @GetMapping(value = "/mods/{idMod}", produces = "application/json")
    public AttackMod findAttackModById(@PathVariable long idMod) {
        return modService.findModById(idMod);
    }

    @GetMapping(value = "/mods/{idFighter}", produces = "application/json")
    public List<AttackMod> findAttackModListByFighter(@PathVariable Fighter fighter) {
        return modService.findAllByFighter(fighter);
    }

    @PostMapping(value = "/mods", produces = "application/json", consumes = "application/json")
    public Long createAttackMod(@RequestBody AttackMod mod) {
        return modService.createMod(mod);
    }

    @PutMapping(value = "/mods", produces = "application/json", consumes = "application/json")
    public AttackMod updateAttackMod(@RequestBody AttackMod mod) {
        return modService.updateMod(mod);
    }

    @DeleteMapping(value = "/mods/{idMod}")
    public void deleteMod(@PathVariable long idMod) {
        modService.deleteMod(idMod);
    }
}
