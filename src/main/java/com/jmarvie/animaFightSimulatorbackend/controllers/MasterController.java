package com.jmarvie.animaFightSimulatorbackend.controllers;

import com.jmarvie.animaFightSimulatorbackend.model.Master;
import com.jmarvie.animaFightSimulatorbackend.services.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MasterController {

    @Autowired
    public MasterService masterService;

    @GetMapping(value="/masters", produces = "application/json")
    public List<Master> findMastersList(){
        return masterService.findAllMasters();
    }

    @GetMapping(value = "/masters/{idMaster}", produces = "application/json")
    public Master findMasterById(@PathVariable long idMaster) {
        return masterService.findMasterById(idMaster);
    }

    @GetMapping(value = "/masters/login/{login}/{password}", produces = "application/json")
    public Master findMasterByLoginAndPassword(@PathVariable String login, @PathVariable String password){
        return masterService.findMasterByLoginAndPassword(login, password);
    }

    @GetMapping(value = "/masters/mail/{email}/{password}", produces = "application/json")
    public Master findMasterByEmailAddressAndPassword(@PathVariable String email, @PathVariable String password){
        return masterService.findMasterByEmailAndPassword(email, password);
    }

    @PostMapping(value="/masters", produces = "application/json", consumes = "application/json")
    public long createMaster(@RequestBody Master master) {
        return masterService.createMaster(master);
    }

    @PutMapping(value="/masters", produces = "application/json", consumes = "application/json")
    public Master updateMaster(@RequestBody Master master) {
        return masterService.updateMaster(master);
    }
}
