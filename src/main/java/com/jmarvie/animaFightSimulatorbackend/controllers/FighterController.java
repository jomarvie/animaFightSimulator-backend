package com.jmarvie.animaFightSimulatorbackend.controllers;

import com.jmarvie.animaFightSimulatorbackend.model.Fighter;
import com.jmarvie.animaFightSimulatorbackend.services.FighterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FighterController {

    @Autowired
    public FighterService fighterService;

    @GetMapping(value = "/fighters/group/{idGroup}", produces = "application/json")
    public List<Fighter> findFightersListByGroup(@PathVariable long idGroup) {
        return fighterService.findAllFightersByGroup(idGroup);
    }

    @GetMapping(value = "/fighters/master/{idMaster}", produces = "application/json")
    public List<Fighter> findFightersListByMaster(@PathVariable long idMaster) {
        return fighterService.findAllFightersByMaster(idMaster);
    }

    @GetMapping(value = "/fighters/{idFighter}", produces = "application/json")
    public Fighter findFighterById(@PathVariable long idFighter) {
        return fighterService.findFighterById(idFighter);
    }

    @PostMapping(value="/fighters", produces = "application/json", consumes = "application/json")
    public long createFighter(@RequestBody Fighter fighter) {
        return fighterService.createFighter(fighter);
    }

    @PutMapping(value="/fighters", produces = "application/json", consumes = "application/json")
    public Fighter updateFighter(@RequestBody Fighter fighter) {
        return fighterService.updateFighter(fighter);
    }

    @DeleteMapping(value = "/fighters/{idFighter}")
    public void deleteFighter(@PathVariable long idFighter){
        fighterService.deleteFighter(idFighter);
    }
}
